# Python HEIC to PNG

## Description
Python Script to convert annoying Apple HEIC Image to PNG Image file format

## Authors and acknowledgment
I got this code from youtube. It is the simplest and minimal unlike others

## How to use
1. Create a venv
2. Install the required packages from requirements.txt
3. Create a input file and add your HEIC images into it
4. Run the bat file. The converted image will be in the same input file