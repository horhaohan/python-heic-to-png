from PIL import Image
from pillow_heif import register_heif_opener
import os

# help(register_heif_opener)
register_heif_opener()
heic_files = [photo for photo in os.listdir(r"input") if '.HEIC' or '.heic' in photo]

os.chdir(r"input")

for photo in heic_files:
    print("Converting: ",photo)
    temp_img = Image.open(photo)
    png_photo = photo.replace('.HEIC','.png')
    temp_img.save(png_photo)
print("Done")